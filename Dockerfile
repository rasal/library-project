FROM alpine
RUN apk add --no-cache curl
RUN curl -o allure-commandline-2.17.3.tgz -Ls https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.17.3/allure-commandline-2.17.3.tgz
RUN tar -zxvf allure-commandline-2.17.3.tgz -C /opt/
RUN ln -s /opt/allure-2.17.3/bin/allure /usr/bin/allure
RUN rm -rf allure-commandline-2.17.3.tgz
RUN apk add --no-cache openjdk8
RUN allure --version
WORKDIR /app
