from src.main.items.borrowed_book import BorrowedBook
from src.main.readers.reader_2 import ar2
from src.main.readers.reader_3 import ar3
from datetime import datetime, timedelta
from prettytable import PrettyTable
from termcolor import colored


class Library:
    def __init__(self):
        self.keys1 = ar2.keys
        self.keys2 = ar3.keys
        self.available_books = ar2.books
        self.borrowed_books = ar3.borrowed_books

    @staticmethod
    def display_books(header: list, data: dict):
        table = PrettyTable()
        for x, y in data.items():
            table.align = "l"
            table.field_names = [*header]
            table.add_row([*y])
        return colored(table, "yellow")

    def add_book(self, book_id: int, title: str, author: str, year: str, language: str, quantity: int):
        self.available_books[book_id] = (title, author, year, language, quantity)
        return self.available_books[book_id]

    def lend_book(self, requested_book: int):
        if requested_book in range(1, len(self.available_books)):
            if self.available_books[requested_book].quantity > 0:
                if requested_book in self.borrowed_books:
                    self.available_books[requested_book].quantity -= 1
                    self.borrowed_books[requested_book].quantity += 1
                    # writer  """"
                else:
                    next_month = datetime.now() + timedelta(days=30)
                    book = self.available_books[requested_book]
                    borrowed_books = \
                        {requested_book: BorrowedBook(
                            book.no, book.title, book.author,
                            book.year, book.language, book.quantity,
                            datetime.now().strftime('%d-%m-%Y'),
                            next_month.strftime('%d-%m-%Y'))}
                    self.borrowed_books.update(borrowed_books)
                    self.available_books[requested_book].quantity -= 1
                    self.borrowed_books[requested_book].quantity = 1
                return (f"You have successfully borrowed "
                        f"\n{self.available_books[requested_book].title} "
                        f"\nfrom {self.available_books[requested_book].author}.")
                # writer  """"
            else:
                return ("Sorry the book you have requested "
                        "is currently not in the library.")
        else:
            return "Sorry, there is no book with this index."

    def return_book(self, returned_book: int):
        if returned_book in range(1, len(self.borrowed_books) + 1):
            if self.borrowed_books[returned_book].quantity > 0:
                self.borrowed_books[returned_book].quantity -= 1
                self.available_books[returned_book].quantity += 1
                # writer ...
                return (f"Thank you for returning borrowed "
                        f"book of name {self.available_books[returned_book].title} "
                        f"from {self.available_books[returned_book].author}.")
            else:
                return ("Sorry the book you have tried to return "
                        "is already in the library.")
        else:
            return "Sorry, there is no book with this index."


library = Library()

# print(library.lend_book(2))
# print(library.display_books(library.keys2, library.borrowed_books))
# print(library.return_book(1))
# print(library.display_books(library.keys2, library.borrowed_books))
