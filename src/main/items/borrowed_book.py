from src.main.items.book import Book


class BorrowedBook(Book):
    def __init__(self, no, title, author, year, language, quantity, borrow_date, return_date):
        super().__init__(no, title, author, year, language, quantity)
        self.borrow_date = borrow_date
        self.return_date = return_date

    def __iter__(self):
        yield self.no
        yield self.title
        yield self.author
        yield self.year
        yield self.language
        yield self.quantity
        yield self.borrow_date
        yield self.return_date

    def __repr__(self):
        return f"{self.no}, {self.title}, {self.author}, " \
               f"{self.year}, {self.language}, {self.quantity}, " \
               f"{self.borrow_date}, {self.return_date}"

    def book_holding(self):
        if self.borrow_date > self.return_date:
            delay = (self.borrow_date - self.return_date).days
            return delay
        else:
            return 0

    def get_book_title(self):
        return self.title

    def get_book_author(self):
        return self.author

    def get_book_year(self):
        return self.year

    def get_book_language(self):
        return self.language

    def get_book_quantity(self):
        return self.quantity

