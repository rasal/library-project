class Book:
    def __init__(self, no, title, author,
                 year, language, quantity):
        self.no = no
        self.title = title
        self.author = author
        self.year = year
        self.language = language
        self.quantity = int(quantity)

    def __iter__(self):
        yield self.no
        yield self.title
        yield self.author
        yield self.year
        yield self.language
        yield self.quantity

    def __repr__(self):
        return f"{self.no}, {self.title}, {self.author}, " \
               f"{self.year}, {self.language}, {self.quantity}"

    def get_book_title(self):
        return self.title

    def get_book_author(self):
        return self.author

    def get_book_year(self):
        return self.year

    def get_book_language(self):
        return self.language

    def get_book_quantity(self):
        return self.quantity
