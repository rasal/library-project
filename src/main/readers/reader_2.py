from src.main.items.book import Book
from settings import path
import csv


class DataReader2:
    def __init__(self, source):
        self.source = source
        self.keys = []
        self.books = {}

    def csv_executor(self):
        with open(self.source, 'r', encoding='ISO-8859-2') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            self.keys = reader.fieldnames
            for position, row in enumerate(reader, start=1):
                books = {position: Book(
                    row['no'], row['title'], row['author'],
                    row['year'], row['language'], row['quantity'])}
                self.books.update(books)


path = path("data/available_books.csv")
ar2 = DataReader2(path)
ar2.csv_executor()

book_1 = ar2.books

# print(book_1[1].title)
# print(ar2.keys)
# print(ar2.books)
