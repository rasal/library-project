from termcolor import colored
from settings import path
from src.main.library import library


class BookReader:
    def __init__(self, source):
        # source = int(input("Please choose book number to read: "))
        self.source = self.book_to_read(source)

    @staticmethod
    def book_to_read(requested_book):
        if requested_book in library.borrowed_books:
            return library.borrowed_books[requested_book].title
        else:
            print(colored("Sorry, not found such book in your repository.", "red"))

    @staticmethod
    def strip_string(title):
        return title.replace('"', '')

    @staticmethod
    def book_reader(source):
        source = path(f"read/{source}.txt")
        with (open(source, mode="r", encoding="utf8")) as file:
            return colored(file.read(), 'yellow')


# book_reader = BookReader()
# if book_reader.source:
#     data = book_reader.strip_string(book_reader.source)
#     print("-" * 200)
#     print(book_reader.book_reader(data))
