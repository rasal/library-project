from src.main.items.borrowed_book import BorrowedBook
from settings import path
import csv


class DataReader3:
    def __init__(self, source):
        self.source = source
        self.keys = []
        self.borrowed_books = {}

    def csv_executor(self):
        with open(self.source, 'r', encoding='ISO-8859-2') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            self.keys = reader.fieldnames
            for position, row in enumerate(reader, start=1):
                books = {position: BorrowedBook(
                    row['no'], row['title'], row['author'],
                    row['year'], row['language'], row['quantity'],
                    str(row['borrow_date']), str(row['return_date']))}
                self.borrowed_books.update(books)


path = path("data/borrowed_books.csv")
ar3 = DataReader3(path)
ar3.csv_executor()

book_1 = ar3.borrowed_books[1]

# print(book_1, "test")
# print(ar3.keys)
# print(ar3.borrowed_books)
