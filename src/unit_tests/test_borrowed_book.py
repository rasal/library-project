from src.main.items.borrowed_book import BorrowedBook
import datetime

book = BorrowedBook(
    2, "In Desert And Wilderness", "Henryk Sienkiewicz", 1911, "Polish", 4,
    datetime.date(2022, 3, 20), datetime.date(2022, 3, 10))


def test_01_book_no():
    assert book.no == 2


def test_02_book_title():
    assert book.title == "In Desert And Wilderness"


def test_03_book_author():
    assert book.author == "Henryk Sienkiewicz"


def test_04_book_year():
    assert book.year == 1911


def test_05_book_language():
    assert book.language == "Polish"


def test_06_book_quantity():
    assert book.quantity == 4


def test_07_quantity_subtract():
    while book.quantity > 0:
        book.quantity -= 1
    assert book.quantity == 0


def test_08_quantity_add():
    while book.quantity < 4:
        book.quantity += 1
    assert book.quantity == 4


# def test_09_book_holding():
#     assert book.book_holding() == 10
