from src.main.library import library
import pytest


@pytest.fixture
def setup():
    yield library
    library.available_books[1].quantity = 4
    library.borrowed_books[1].quantity = 1


def test_01_add_book():
    assert library.add_book(
        9, "Sun Tzu", "Sunzi", "500BCE", "Chinese", 1) \
           == ('Sun Tzu', 'Sunzi', '500BCE', 'Chinese', 1)


@pytest.mark.parametrize("actual, value, expected", [
    (1, 1, 'You have successfully borrowed \n"Pan Tadeusz" \nfrom Adam Mickiewicz.'),
    (1, 0, 'Sorry the book you have requested is currently not in the library.'),
    (0, 1, 'Sorry, there is no book with this index.')])
def test_02_lend_book_alerts(setup, actual, value, expected):
    if actual != 0:
        library.available_books[actual].quantity = value
    assert library.lend_book(actual) == expected


@pytest.mark.parametrize("actual, expected_library, expected_user",
                         [(1, 3, 2), (2, 3, 1), (4, 1, 1), (6, 0, 1)])
def test_03_lend_book_quantity(actual, expected_library, expected_user):
    library.lend_book(actual)
    assert library.available_books[actual].quantity == expected_library
    assert library.borrowed_books[actual].quantity == expected_user


@pytest.mark.parametrize("actual, value, expected", [
    (1, 1, 'Thank you for returning borrowed book of name "Pan Tadeusz" from Adam Mickiewicz.'),
    (1, 0, 'Sorry the book you have tried to return is already in the library.'),
    (0, 1, 'Sorry, there is no book with this index.')])
def test_04_return_book_alerts(setup, actual, value, expected):
    if actual != 0:
        library.borrowed_books[actual].quantity = value
    assert library.return_book(actual) == expected


def test_05_return_book_quantity(actual=1, expected_user=0, expected_library=5):
    library.return_book(actual)
    assert library.borrowed_books[actual].quantity == expected_user
    assert library.available_books[actual].quantity == expected_library

