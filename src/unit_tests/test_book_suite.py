from src.main.items.book import Book
book = Book(1, "Pan Tadeusz", "Adam Mickiewicz", 1800, "Polish", 1)


def test_01_book_no():
    assert book.no == 1


def test_02_book_title():
    assert book.title == "Pan Tadeusz"


def test_03_book_author():
    assert book.author == "Adam Mickiewicz"


def test_04_book_year():
    assert book.year == 1800


def test_05_book_language():
    assert book.language == "Polish"


def test_06_book_quantity():
    assert book.quantity == 1


def test_07_quantity_subtract():
    while book.quantity > 0:
        book.quantity -= 1
    assert book.quantity == 0


def test_08_quantity_add():
    while book.quantity < 4:
        book.quantity += 1
    assert book.quantity == 4
