# By: Adam Kurek
# Date: 2021-02-28
# File Name: library-project
# Description: library simulator


from datetime import datetime, timedelta
from bcrypt import hashpw, gensalt, checkpw
from prettytable import PrettyTable
from termcolor import colored
import mysql.connector
import random
import csv
import re

connection = mysql.connector.connect(
    user='admin', password='*****', host='127.0.0.1',
    database='library_db', auth_plugin='mysql_native_password')
cursor = connection.cursor()

book_list = []
"""Import a list of data from a CSV file"""
with open('data/readings.csv') as collection:
    book_file = csv.DictReader(collection, delimiter=';')
    for elements in book_file:
        book_list.append(elements)


def timer():
    """Shows the current date and time."""
    start_date = datetime.now().strftime(f"%Y-%m-%d %H:%M:%S")
    end_date = (datetime.now() + timedelta(days=30)).replace(microsecond=0)
    return start_date, end_date


def welcome():
    """Starting text."""
    print('\n' + '*' * 24 + '  WELCOME TO PYTHON LIBRARY  ' + '*' * 23)
    print('\nPlease select operation:'
          '\n1) login\n2) register')
    operation = get_operation_number('\nOperation: ')
    if operation == 1:
        login()
    elif operation == 2:
        register()


def get_operation_number(text):
    """Checking if operation input is an integer."""
    while True:
        try:
            return int(input(text))
        except ValueError:
            print(colored('Operation must be an integer.', 'red'))


def get_name(text):
    """Checking if name input is a string of letters."""
    while True:
        name = input(text).capitalize()
        if name.isalpha():
            return name
        else:
            print(colored('All characters are not string of letters.', 'red'))


def get_password(text):
    """Checking if password input is an alphanumeric."""
    while True:
        password = input(text)
        if password.isalnum():
            return password
        else:
            print(colored('All characters are not alphanumeric.', 'red'))


def password_checker(text):
    """Checking if the conditions for the correct password are met."""
    while True:
        password = input(text)
        if len(password) < 7:
            print(colored('Your password must have at lest 8 letters', 'red'))
        elif re.search(r'[0-9]{4,}', password) is None:
            print(colored('Your password must contain at least four numbers.', 'red'))
        elif re.search('[A-Z]', password) is None:
            print(colored('Your password must have a capital letter.', 'red'))
        else:
            return password


def register():
    """New account registration process."""
    print('\n' + '-' * 32 + '  REGISTER  ' + '-' * 32)
    print('Please enter your details.')
    account_number = random.randint(10000, 20000)
    name = get_name('\nName: ')
    surname = get_name('\nSurname: ')
    password_1 = password_checker('\nPassword: ')
    password_2 = password_checker('\nRepeat password: ')
    if password_2 != password_1:
        print(colored('Sorry the passwords not match, please try again.', 'red'))
        register()
    else:
        account = f'{name}_{surname}'
        print(colored('Encryption may take several seconds.', 'yellow'))
        hashed = hashpw(password_1.encode('utf-8'), gensalt(16))
        database_register(account_number, hashed.decode('utf-8'), account)
        print(colored('Registration process succeed.', 'blue'))
        print(colored(f'\nuser number: {account_number} \npassword: '
                      f'{password_1} \nname: {name} \nsurname: {surname}', 'yellow'))
        welcome()


def database_register(account_number, hashed, account):
    """Save new account data into the database."""
    registration = f'INSERT INTO users (account_name, account_number, password) ' \
                   f'VALUES ("{account}", {account_number}, "{hashed}")'
    file = f'CREATE TABLE `library_db`.`{account}` (`bookID` INT NOT NULL AUTO_INCREMENT,' \
           f'`title` VARCHAR(45) NOT NULL, `author` VARCHAR(45) NOT NULL, `year` INT NOT NULL,' \
           f'`language` VARCHAR(45) NOT NULL, `borrow_date` DATE NOT NULL, `return_date` DATE NOT NULL,' \
           f'PRIMARY KEY (`bookID`))'
    cursor.execute(registration)
    cursor.execute(file)
    connection.commit()


def login():
    """Validation of the login and password."""
    print('\n' + '-' * 34 + '  LOGIN  ' + '-' * 33)
    account_number = get_operation_number('\nUser number: ')
    password = get_password('Password: ').encode('utf-8')
    account, access = password_database(account_number)
    print(colored('Decryption may take several seconds.', 'yellow'))
    if checkpw(password, access.encode('utf-8')):
        print(colored('Login successful!', 'blue'))
        menu(account)
    else:
        print(colored('Wrong login or password.', 'red'))
        login()


def password_database(account_number):
    """Reading data from the database."""
    password_data = f'SELECT account_name, account_number, password FROM users ' \
                    f'WHERE account_number = {account_number}'
    cursor.execute(password_data)
    query_work = cursor.fetchall()
    if query_work:
        for row in query_work:
            return row[0], row[2]
    else:
        print(colored('Wrong login or password.', 'red'))
        login()


def menu(account):
    """Function dashboard."""
    print('\n' + '-' * 33, '  MENU  ', '-' * 33)
    print('Please choose operation:'
          '\n1) status\n2) borrow book\n3) return book\n4) exit')
    while True:
        operation = get_operation_number('\nOperation: ')
        """Choosing operation from function dashboard."""
        if operation == 1:
            database_status(account)
        elif operation == 2:
            choosing_sort(account)
        elif operation == 3:
            book_return(account)
        elif operation == 4:
            quit('Thank you for using our service, see you next time.')
        else:
            print(colored('Please choose correct number of operation.', 'red'))


def database_status(account):
    """Showing account status."""
    print('\n' + '-' * 32, '  STATUS  ', '-' * 32)
    status = f'SELECT title, author, year, ' \
             f'language, borrow_date, return_date FROM {account}'
    cursor.execute(status)
    results = cursor.fetchall()
    if not results:
        print(colored('You have not borrowed any book yet.', 'yellow'))
        menu(account)
    else:
        no = 0
        table = PrettyTable()
        print('user:', account.upper())
        for record in results:
            no += 1
            table.field_names = ['no', 'title', 'author', 'year', 'language', 'borrow_date', 'return_date']
            table.add_row([f'{no}', f'{record[0]}', f'{record[1]}', f'{record[2]}',
                           f'{record[3]}', f'{record[4]}', f'{record[5]}'])
        print(colored(table, 'yellow'))
        reader(account, results)


def reader(account, results):
    """Function of reading borrowed data."""
    print('\nChoose operation.')
    print('1. read online\n2. main menu')
    operation = get_operation_number('\nOperation: ')
    if operation == 1:
        print('\nPlease select number of book to read.')
        read = get_operation_number('Book no: ')
        try:
            book = results[read - 1][0]
            print(colored(f'\n{book}', 'blue'))
            file = open(f'read/{book}.txt', 'r')
            print(colored(file.read(), 'yellow'))
            menu(account)
        except IndexError:
            print(colored('There is no such book on the list.', 'red'))
            reader(account, results)
    elif operation == 2:
        menu(account)
    else:
        print(colored('Please choose correct number of operation.', 'red'))
        reader(account, results)


def choosing_sort(account):
    """Choosing method of sorting data."""
    print('\n' + '-' * 30, '  BOOK BORROW  ', '-' * 29)
    while True:
        sort_method = ['no', 'title', 'author', 'year', 'language']
        print('Enter one of the below method of sorting:')
        print(' '.join(sort_method))
        sort = input('\nSort: ')
        if sort in sort_method:
            print('\nChoose method of segregation:')
            print('"f" for (ascendant/ A-z)'
                  '\n"t" for (descendant/ Z-a)')
            choice = input('\nSegregation: ').lower()
            if choice == 'f':
                segregation = False
                sorting(sort, segregation, account)
            elif choice == 't':
                segregation = True
                sorting(sort, segregation, account)
            else:
                print(colored('Please choose an available operation.', 'red'))
                choosing_sort(account)
        else:
            print(colored('Please choose correct method of sorting.', 'red'))
            choosing_sort(account)


def sorting(sort, segregation, account):
    """Sorting data based on selected options."""
    orderly = sorted(book_list, key=lambda var: var[sort], reverse=segregation)
    table = PrettyTable()
    for record in orderly:
        table.field_names = ['no', 'title', 'author', 'year', 'language']
        table.add_row([f'{record["no"]}', f'{record["title"]}', f'{record["author"]}',
                       f'{record["year"]}', f'{record["language"]}'])
    print(colored(table, 'yellow'))
    choose_book(account)


def choose_book(account):
    """Choosing book to borrow."""
    print('\nPlease choose book number you want to borrow.'
          '\n(you can have maximum 4 at once)')
    start_date, end_date = timer()
    while True:
        book = get_operation_number('\nBook: ')
        choice = book_list[book - 1]['title']
        print(colored(f'Your choice is {choice}', 'yellow'))
        print('\nSelect operation: ')
        print('1. finalize\n2. choose again\n3-0. main menu')
        operation = get_operation_number('\nOperation: ')
        if operation == 1:
            database_borrow(account, choice, book_list[book - 1]['author'],
                            book_list[book - 1]['year'], book_list[book - 1]['language'],
                            start_date, end_date)
            print(colored(f'\nYou have borrowed book:', 'blue'))
            print(colored(f'{choice}', 'yellow'))
            menu(account)
        elif operation == 2:
            choosing_sort(account)
        else:
            menu(account)


def borrow_control(account):
    """Checking the limit of borrowed data."""
    results = f'SELECT COUNT(*), MAX(bookID) FROM {account}'
    cursor.execute(results)
    for count, max_id in cursor:
        if count in range(1, 4):
            return max_id
        elif count == 0:
            return 0
        else:
            print(colored('You reach maximum number of borrowed data.', 'red'))
            menu(account)


def database_borrow(account, title, author, year, language, borrow_date, return_date):
    """Saving the borrowed book in the database."""
    borrow = f'INSERT INTO {account} (bookID, title, author, year, language, borrow_date, return_date)' \
             f' VALUES ({borrow_control(account) + 1}, {title}, "{author}", {year}, "{language}", ' \
             f'"{borrow_date}", "{return_date}")'
    cursor.execute(borrow)
    connection.commit()


def book_return(account):
    """Choosing book to return."""
    print('\n' + '-' * 30, '  BOOK RETURN  ', '-' * 29)
    return_book = f'SELECT title, author, return_date FROM {account}'
    cursor.execute(return_book)
    results = cursor.fetchall()
    if not results:
        print(colored('You have not borrowed any book yet.', 'yellow'))
        menu(account)
    else:
        no = 0
        table = PrettyTable()
        for record in results:
            no += 1
            table.field_names = ['no', 'title', 'author', 'return_date']
            table.add_row([f'{no}', f'{record[0]}', f'{record[1]}', f'{record[2]}'])
        print(colored(table, 'yellow'))
        print('\nPlease select number of book to return.')
        back = get_operation_number('\nBook no: ')
        try:
            title = results[back - 1][0]
            database_return(account, title)
            print(colored(f'You have returned book:', 'blue'))
            print(colored(f'{title}', 'yellow'))
            menu(account)
        except IndexError:
            print(colored('There is no such book on the list.', 'red'))
            menu(account)


def database_return(account, title):
    """Saving return of book in the database."""
    back = f'DELETE FROM {account} WHERE title = "{title}"'
    cursor.execute(back)
    connection.commit()


def main():
    welcome()


if __name__ == '__main__':
    main()
