import os


def path(source):
    root_path = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(root_path, f"{source}")
