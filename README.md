# LIBRARY MANAGEMENT - project

---
### Install all required dependencies:
```
pip install -r requirements.txt
```

### Features:

First version in functional programming style is dated on: 2021-02-28 <br>
second version in OOP style is for training new Python features dated on late 2022.

Project have test automation from Gitlab CI/CD with Docker Python image 3.10 <br>
and Docker build from Dockerfile to execute command for generating Allure test report.

After successful building CI/CD pipeline script generating Allure report <br>
which can be visible from website level on index.html file in artifacts folder.

___

FUNCTIONS
___
1. The new password is verified in terms of a capital letter, minimum length of 7 characters and 4 digits.
2. In addition, the password is encrypted with a special algorithm based on randomly generated salt <br>
and hashing a string entered by the user. Verification consists in comparing the encrypted string with the one <br>
saved in the database. Login provides automatic decryption of the encrypted password based on the built-in salt.
3. You can create a new account and login with a randomly generated account number. Furthermore, the encryption <br>
and decryption process takes several seconds. All this makes the password very difficult to crack and the usual <br>
rainbow table cannot do that. Each time a new account is set up, it automatically creates a file in the database.
4. After logging in, you can use convenient menu where you can choose to display the status, <br>
borrow a book, return the book and exit the program.
5. The status display shows basic information about the books on loan including the date of return.
6. You can also display a text of borrowed books in a given language.
7. The maximum number of books that can be kept at once is 4.
8. In the borrow book option, you can sort books in ascending or descending order <br>
based on the title, author, year of creation or language of book.

INSTRUCTION FOR OLD VERSION
___
To integrate the database with MySQL import the library_db by checking <br>
the dump option in the admin panel and selecting the appropriate path.